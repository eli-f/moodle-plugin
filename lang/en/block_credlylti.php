<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'chat', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   mod_chat
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['method'] = 'Credly method';
$string['methoddaemon'] = 'Credly server daemon';
$string['methodnormal'] = 'Normal method';
$string['methodajax'] = 'Ajax method';
$string['modulename'] = 'Credly LTI';
$string['modulename_help'] = '';
$string['modulename_link'] = 'mod/credly/view';
$string['modulenameplural'] = 'Credly';

$string['apikey'] = 'Credly App key';
$string['apikeydesc'] = 'The App key assigned to your enterprise issuer. To find the app key, log in to https://developers.credly.com';
$string['apisecret'] = 'Credly App secret';
$string['apisecretdesc'] = 'The App secret assigned to your enterprise issuer. To find the app secret, log in to https://developers.credly.com';
$string['enterpriseurl'] = 'Credly Enterprise domain';
$string['enterpriseurldesc'] = 'The domain name assigned to your Credly Enterprise site.';
$string['integrationid'] = 'Integration ID';
$string['integrationiddesc'] = 'The integration ID assigned by Credly.';

$string['entrypoint'] = 'My Credentials';
